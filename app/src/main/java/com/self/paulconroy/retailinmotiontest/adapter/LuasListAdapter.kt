package com.self.paulconroy.retailinmotiontest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.self.paulconroy.retailinmotiontest.R
import com.self.paulconroy.retailinmotiontest.model.LuasData
import com.self.paulconroy.retailinmotiontest.ui.main.MainActivity
import com.self.paulconroy.retailinmotiontest.utils.Constants

/**
 * LuasListAdapter Recyclerview will hold and recycle item views containing each available Luas
 */
class LuasListAdapter(private val activity: MainActivity,
                      private var availableLuasList: List<LuasData>) : RecyclerView.Adapter<LuasListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LuasListAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.luas_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return availableLuasList.size
    }

    override fun onBindViewHolder(holder: LuasListAdapter.ViewHolder, position: Int) {
        holder.tvDestination.text = this.availableLuasList[position].destination

        if (this.availableLuasList[position].dueMins == Constants.instance.DUE_STRING) {
            holder.tvDueIn.text = this.availableLuasList[position].dueMins
        } else {
            holder.tvDueIn.text = activity.resources.getString(R.string.x_mins,
                    this.availableLuasList[position].dueMins)
        }

    }

    /**
     * Used to refresh the list of luas items.
     */
    fun amendItems(amendedLuasList: List<LuasData>) {
        this.availableLuasList = amendedLuasList
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDestination: TextView = itemView.findViewById(R.id.tvDestination)
        val tvDueIn: TextView = itemView.findViewById(R.id.tvDueIn)
    }
}