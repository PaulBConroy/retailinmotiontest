package com.self.paulconroy.retailinmotiontest.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import com.self.paulconroy.retailinmotiontest.R
import com.self.paulconroy.retailinmotiontest.ui.main.MainActivity

/**
 * NoConnectionDialog dialog informs user that an issue has occurred via lack of internet.
 */
class NoConnectionDialog(private val activity: MainActivity) {

    private val dialog: Dialog = Dialog(activity)
    var btnTryAgain: TextView

    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_no_connection)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.attributes.windowAnimations = R.style.DialogAnimation

        btnTryAgain = dialog.findViewById(R.id.btnTryAgain)
        setListeners()
    }

    private fun setListeners() {
        btnTryAgain.setOnClickListener {
            activity.getPresenter().retrieveData()
            dismissDialog()
        }
    }

    fun showDialog() {
        dialog.show()
    }

    private fun dismissDialog() {
        dialog.dismiss()
    }
}