package com.self.paulconroy.retailinmotiontest.utils

/**
 * A Singleton Constants class used to house the constant values used by the application.
 */

class Constants {

    companion object {
        val instance = Constants()
    }

    /**
     * String constant of the URL used to fetch the Luas station data.
     */
    val BASE_URL = "http://luasforecasts.rpa.ie/xml/"

    /**
     * String constant value of "Stillorgan"
     */
    val STILLORGAN = "Stillorgan"

    /**
     * String constant value of "Marlborough"
     */
    val MARLBOROUGH = "Marlborough"

    /**
     * String constant value of "Destination"
     */
    val DESTINATION = "Destination"

    /**
     * String constant value of "direction"
     */
    val DIRECTION = "direction"

    /**
     * String constant value of "dueMins"
     */
    val DUE_MINS = "dueMins"

    /**
     * String constant value of "DUE"
     */
    val DUE_STRING = "DUE"

    /**
     * String constant value of "name"
     */
    val NAME = "name"

    /**
     * String constant value of "Outbound"
     */
    val OUTBOUND = "Outbound"

    /**
     * String constant value of "Inbound"
     */
    val INBOUND = "Inbound"

    /**
     * String constant value of Time Format used in General Utils
     */
    val TIME_FORMAT = "HH:mm"

    /**
     * String constant value of comparable time used in General Utils
     */
    val COMPARE_TIME_ONE = "00:00"

    /**
     * String constant value of comparable time used in General Utils
     */
    val COMPARE_TIME_TWO = "12:01"



}