package com.self.paulconroy.retailinmotiontest.ui.main

import com.self.paulconroy.retailinmotiontest.model.LuasData

/**
 * The view allows the presenter implementation to interact with the activity UI.
 */
interface MainView {

    /**
     * Update the recyclerview list with the data retrieved from the API call.
     *
     * @param luasDataList the list of Luas data.
     */
    fun updateLuasList(luasDataList: List<LuasData>)

    /**
     * Set the direction title on the UI for the user.
     *
     * @param direction the direction string.
     */
    fun setDirectionTitle(direction: String)

    /**
     * Set the station title on the UI for the user.
     *
     * @param station the station string.
     */
    fun setStationTitle(station: String)

    /**
     * If there is a problem with the API call, inform the user.
     */
    fun displayNoConnectionDialog()
}