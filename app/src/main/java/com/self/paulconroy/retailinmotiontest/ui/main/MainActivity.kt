package com.self.paulconroy.retailinmotiontest.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.self.paulconroy.retailinmotiontest.R
import com.self.paulconroy.retailinmotiontest.adapter.LuasListAdapter
import com.self.paulconroy.retailinmotiontest.dialog.NoConnectionDialog
import com.self.paulconroy.retailinmotiontest.model.LuasData

class MainActivity : AppCompatActivity(), MainView {

    private var presenter: MainPresenter? = null
    private var availableLuasAdapter: LuasListAdapter? = null
    private lateinit var rvAvailableTrainList: RecyclerView
    private var noConnectionDialog: NoConnectionDialog? = null
    lateinit var pbProgress: ProgressBar
    lateinit var tvDirection: TextView
    lateinit var tvToolbarTitle: TextView
    lateinit var fabRefresh: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvDirection = findViewById(R.id.tvDirection)
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle)
        pbProgress = findViewById(R.id.pbProgress)
        fabRefresh = findViewById(R.id.fab)
        fabRefresh.setOnClickListener { handleFabClick() }
        rvAvailableTrainList = findViewById(R.id.rvRecyclerView)

    }

    override fun onResume() {
        super.onResume()
        getPresenter().retrieveData()
    }

    /**
     * Returns a single instance of the Main Presenter class
     */
    fun getPresenter(): MainPresenterImp {
        if (presenter == null) {
            presenter = MainPresenterImp(this)
        }
        return presenter as MainPresenterImp
    }

    override fun updateLuasList(luasDataList: List<LuasData>) {
        pbProgress.visibility = View.GONE
        if (availableLuasAdapter == null) {
            availableLuasAdapter = LuasListAdapter(this, luasDataList)
            rvAvailableTrainList.layoutManager = LinearLayoutManager(this)
            rvAvailableTrainList.adapter = availableLuasAdapter
            availableLuasAdapter!!.notifyDataSetChanged()
            rvAvailableTrainList.invalidate()
        } else {
            availableLuasAdapter!!.amendItems(luasDataList)
            availableLuasAdapter!!.notifyDataSetChanged()
        }
    }

    override fun setDirectionTitle(direction: String) {
        tvDirection.text = direction
    }

    override fun setStationTitle(station: String) {
        tvToolbarTitle.text = station
    }

    override fun displayNoConnectionDialog() {
        noConnectionDialog = NoConnectionDialog(this)
        noConnectionDialog!!.showDialog()
    }

    private fun handleFabClick() {
        pbProgress.visibility = View.VISIBLE
        getPresenter().retrieveData()
    }
}
