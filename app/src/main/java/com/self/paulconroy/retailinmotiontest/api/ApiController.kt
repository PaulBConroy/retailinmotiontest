package com.self.paulconroy.retailinmotiontest.api

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The controller is used to host the methods used for Retrofit to retrieve Luas Station information.
 */
interface ApiController {

    /**
     * Fetch the results for the Marlborough data.
     */
    @GET("get.ashx?action=forecast&stop=mar&encrypt=false")
    fun getMarlboroughData(): Observable<String>

    /**
     * Fetch the results for the Stillorgan data.
     */
    @GET("get.ashx?action=forecast&stop=sti&encrypt=false")
    fun getStillorganData(): Observable<String>

}