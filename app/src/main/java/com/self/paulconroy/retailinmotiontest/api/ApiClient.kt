package com.self.paulconroy.retailinmotiontest.api

import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.self.paulconroy.retailinmotiontest.utils.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * API Client used to set the controller and invoke controller methods.
 */
class ApiClient : Callback<String> {

    lateinit var results: String
    lateinit var apiController: ApiController

    override fun onFailure(call: Call<String>, t: Throwable) {
        Log.e("ApiClient: Error ", t.message)
    }

    override fun onResponse(call: Call<String>, response: Response<String>) {
        results = response.body()!!
    }

    init {
        start()
    }

    /**
     * Constructs the api controller
     */
    fun start() {

        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(getBaseURL())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()

        apiController = retrofit.create(ApiController::class.java)
    }

    /**
     * The base URL string to retrieve the station data.
     */
    private fun getBaseURL(): String {
        return Constants.instance.BASE_URL
    }
}