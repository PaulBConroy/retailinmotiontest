package com.self.paulconroy.retailinmotiontest.ui.main

import android.util.Log
import com.self.paulconroy.retailinmotiontest.api.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jsoup.Jsoup
import com.self.paulconroy.retailinmotiontest.model.LuasData
import com.self.paulconroy.retailinmotiontest.utils.Constants
import com.self.paulconroy.retailinmotiontest.utils.GeneralUtils
import org.jsoup.select.Elements


/**
 * The presenter implementation implements logic methods. No UI methods are to be placed here,
 * only logic, business, api calls etc.
 */
class MainPresenterImp(val view: MainView) : MainPresenter {


    override fun retrieveData() {
        //Depending on the time of day, a particular API call is used.
        when (GeneralUtils.instance.getDirectionBasedOnTime()) {
            Constants.instance.MARLBOROUGH -> getMarlboroughData()
            Constants.instance.STILLORGAN -> getStillorganData()
        }
    }

    /**
     * API call to retrieve OUTBOUND luas data from Marlborough station.
     * Pass the OUTBOUND data to the `parseElementData()` function to parse and filter relevant data.
     * If an issue occurs, inform the user.
     */
    private fun getMarlboroughData() {
        val compositeDisposable = CompositeDisposable()
        val disposableObserver = ApiClient().apiController.getMarlboroughData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {
                        Log.e("========== onComplete ", "Complete")
                        compositeDisposable.dispose()
                    }

                    override fun onNext(t: String) {
                        Log.d("========== onNext ", "Response data received")
                        val document = Jsoup.parse(t)
                        val elementsDir = document.select(Constants.instance.DIRECTION)
                        for (element in elementsDir) {
                            when (element.attr(Constants.instance.NAME)) {
                                Constants.instance.OUTBOUND -> parseElementData(element.children())
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.e("========== onError ", e.message)
                        view.displayNoConnectionDialog()
                        compositeDisposable.dispose()
                    }

                })
        view.setDirectionTitle(Constants.instance.OUTBOUND)
        view.setStationTitle(Constants.instance.MARLBOROUGH)
        compositeDisposable.add(disposableObserver)
    }

    /**
     * API call to retrieve INBOUND luas data from Marlborough station.
     * Pass the INBOUND data to the `parseElementData()` function to parse and filter relevant data.
     * If an issue occurs, inform the user.
     */
    private fun getStillorganData() {
        val compositeDisposable = CompositeDisposable()
        val disposableObserver = ApiClient().apiController.getStillorganData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {
                        Log.e("========== onComplete ", "Complete")
                        compositeDisposable.dispose()
                    }

                    override fun onNext(t: String) {
                        Log.d("========== onNext ", "Response data received")
                        val document = Jsoup.parse(t)
                        val elementsDir = document.select(Constants.instance.DIRECTION)
                        for (element in elementsDir) {
                            when (element.attr(Constants.instance.NAME)) {
                                Constants.instance.INBOUND -> parseElementData(element.children())
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.e("========== onError ", e.message)
                        view.displayNoConnectionDialog()
                        compositeDisposable.dispose()
                    }

                })
        view.setDirectionTitle(Constants.instance.INBOUND)
        view.setStationTitle(Constants.instance.STILLORGAN)
        compositeDisposable.add(disposableObserver)
    }

    /**
     * Parse the element childNode data from the API and pull the `dueMins` and `destination` data
     * from each childNode. Compile a list of this data and pass it to the UI to update the
     * recycler view.
     *
     * @param childNodes The childNode elements from the API call.
     */
    fun parseElementData(childNodes: Elements) {
        val luasList: MutableList<LuasData> = mutableListOf()
        for (childNode in childNodes) {
            val luasData = LuasData()
            luasData.dueMins = childNode.attr(Constants.instance.DUE_MINS)
            luasData.destination = childNode.attr(Constants.instance.DESTINATION)
            luasList.add(luasData)
        }

        view.updateLuasList(luasList)
    }

}