package com.self.paulconroy.retailinmotiontest.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * A Singleton General Utility class used to handle time logic.
 */
class GeneralUtils {

    companion object {
        val instance = GeneralUtils()
    }

    /**
     * Based on the time of day, a particular endpoint will be called on the API.
     *
     * This function takes the current time and checks certain conditions:
     * If it is between 00:00 and 12:00 -> invoke a call to the Marlborough station.
     * Otherwise -> invoke a call to the Stillorgan station.
     *
     * @return The Station string to determine what API endpoint to call.
     */
    fun getDirectionBasedOnTime(): String {
        val now = Calendar.getInstance()
        val hour = now.get(Calendar.HOUR_OF_DAY)
        val minute = now.get(Calendar.MINUTE)

        val date = parseDate(hour.toString() + ":" + minute)
        val timeCompareOne = parseDate(Constants.instance.COMPARE_TIME_ONE)
        val timeCompareTwo = parseDate(Constants.instance.COMPARE_TIME_TWO)

        if (date == timeCompareOne || date.after(timeCompareOne)
                && date.before(timeCompareTwo)) {
            return Constants.instance.MARLBOROUGH
        }

        return Constants.instance.STILLORGAN
    }

    /**
     * Parse the time string uses the specified time format into a Date object.
     *
     * @param time The time string.
     * @return The constructed Date object.
     */
    private fun parseDate(time: String): Date {
        val inputFormat = Constants.instance.TIME_FORMAT
        val inputParser = SimpleDateFormat(inputFormat, Locale.US)
        return try {
            inputParser.parse(time)
        } catch (e: java.text.ParseException) {
            Date(0)
        }

    }
}