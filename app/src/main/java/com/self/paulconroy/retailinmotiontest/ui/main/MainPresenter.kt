package com.self.paulconroy.retailinmotiontest.ui.main

/**
 * The presenter is used to invoke logic methods from the activity
 *
 */
interface MainPresenter {

    /**
     * Retrieve the data from the API
     */
    fun retrieveData()

}